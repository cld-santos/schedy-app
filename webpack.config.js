var webpack = require('webpack');
var path = require('path');

var BUILD_DIR = path.resolve(__dirname, 'build');
var APP_DIR = path.resolve(__dirname, 'src/app');
// var BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

var config = {
  entry: APP_DIR + '/index.jsx',
  output: {
    path: BUILD_DIR,
    filename: 'bundle.js'
  },
  module: {
    rules: [
      {test: /\.(js|jsx)$/, use: 'babel-loader'},
      {
        test: /\.css$/,
        use: [
          { loader: 'style-loader' },
          { loader: 'css-loader' }
        ]
      },
      {test: /\.json$/, loader: 'json-loader'},
      {test: /\.(png)$/, use: 'url-loader'},
      {test: /\.(jpg|ttf|woff|eot|svg|woff2)$/, use: 'file-loader'}
    ]
  }
  //,plugins: [new BundleAnalyzerPlugin()]
};

module.exports = config;

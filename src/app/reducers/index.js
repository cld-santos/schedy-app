import { combineReducers } from 'redux';

const credential = (state = [], action) => {
  switch (action.type) {
    case 'AUTHENTICATED':
      return {
        authenticated: true,
        id: action.id,
        username: action.username,
        token: action.token
      };
    case 'NON_AUTHENTICATED':
      return {
        authenticated: false
      };
    case 'LOGOUT':
      return {
        username: action.username,
        authenticated: false
      };
    default:
      return state;
  }
};

const user = (state = [], action) => {
  switch (action.type) {
    case 'CREATE_USER':
    case 'FAIL_CREATE_USER':
      return action;
    default:
      return state;
  }
};

const users = (state = [], action) => {
  switch (action.type) {
    case 'CREATE_USER':
      return {
        'items': [...state.items, user(undefined, action)],
        'created': true
      };
    case 'FAIL_CREATE_USER':
      return {
        'items': [...state.items, user(undefined, action)],
        'created': false
      };
    case 'LIST_USER':
      return {
        'items': action.items
      };
    default:
      return {
        'items': state.items
      };
  }
};

const service = (state = [], action) => {
  switch (action.type) {
    case 'GET_SERVICE':
      return {
        'item': action.item
      };
    case 'LIST_SERVICES':
      return {
        'items': action.items
      };
    case 'LIST_PROFESSIONALS':
      return {
        'items': state.items,
        'professionals': action.professionals
      };
    case 'CREATE_SERVICE':
    case 'UPDATE_SERVICE':
      return {
        'items': [...state.items, action.items],
        'created': action.created
      };
    case 'FAIL_CREATE_SERVICE':
    case 'FAIL_UPDATE_SERVICE':
      return {
        'items': state.items,
        'created': action.created
      };
    default:
      return state;
  }
};

const servicesByProfessional = (state = [], action) => {
  switch (action.type) {
    case 'LIST_SERVICES_BY_PROFESSIONAL':
      if (action.servicesByProfessional)
        return {
          'items': action.servicesByProfessional
        };
      else
        return {
          'items': []
        };
    default:
      return state;
  }
};
const client = (state = [], action) => {
  switch (action.type) {
    case 'LIST_CLIENT':
      return {
        'items': action.items
      };
    case 'CREATE_CLIENT':
      return action;
    case 'FAIL_CREATE_CLIENT':
      return action;
    default:
      return state;
  }
};

const note = (state = [], action) => {

  switch (action.type) {
    case 'LIST_NOTE':
      return {
        'items': action.items
      };
    case 'CREATE_NOTE':
      return {
        'items':  [ action.item, ...state.items],
        'created': true
      };
    case 'FAIL_CREATE_NOTE':
      return {
        'items': action,
        'created': false
      };
    default:
      return state;
  }
};

const schedy = (state = [], action) => {
  switch (action.type) {
    case 'TITLE_CHANGED':
      return {
        'title': action.title
      };
    case 'VIEW_CHANGED':
      return {
        'mode': action.mode
      };
    default:
      return state;
  }
};

const loginApp = combineReducers({
  schedy,
  credential,
  users,
  service,
  note,
  client,
  servicesByProfessional
});

export default loginApp

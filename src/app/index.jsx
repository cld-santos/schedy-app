import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { browserHistory, Switch, Redirect, Link, HashRouter, Route } from 'react-router-dom';
import thunkMiddleware from 'redux-thunk'
import {compose, createStore, applyMiddleware } from 'redux';
import {persistStore, autoRehydrate} from 'redux-persist'
import { Provider } from 'react-redux';
import { authenticated} from './actions/auth.js';
import reducer from './reducers';

import Toolbar from './containers/Toolbar.jsx';
import SideMenu from './containers/SideMenu.jsx';
import Authenticate from './containers/Authenticate.jsx';
import Users from './containers/ManageUsers.jsx';
import Services from './containers/Services.jsx';
import ManageClients from './containers/ManageClients.jsx';
import ServiceDetail from './containers/ServiceDetail.jsx';
import HomeServices from './containers/HomeServices.jsx';

import '@material/button/dist/mdc.button.css';
import '@material/card/dist/mdc.card.css';
import '@material/drawer/dist/mdc.drawer.css';
import '@material/elevation/dist/mdc.elevation.css';
import '@material/fab/dist/mdc.fab.css';
import '@material/form-field/dist/mdc.form-field.css';
import '@material/list/dist/mdc.list.css';
import '@material/menu/dist/mdc.menu.css';
import '@material/textfield/dist/mdc.textfield.css';
import '@material/toolbar/dist/mdc.toolbar.css';
import '@material/dialog/dist/mdc.dialog.css';
import '@material/layout-grid/dist/mdc.layout-grid.css';
import '@material/select/dist/mdc.select.css';
import '@material/snackbar/dist/mdc.snackbar.css';
import '@material/typography/dist/mdc.typography.css';


const store = createStore(
    reducer,
    undefined,    
    compose(
      applyMiddleware(thunkMiddleware),
      autoRehydrate()
    )    
);
persistStore(store);

const unSub = store.subscribe(() => {
    console.log(store.getState());
});

class Schedy extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isMenuOpen: false,
      mainTitle: 'Schedy',
      contentMode: 'list',
      isActionButtonsVisible: false
    }
    this.toggleSideMenu = this.toggleSideMenu.bind(this);
  }

  toggleSideMenu() {
    this.setState({
      isMenuOpen: !this.state.isMenuOpen
    });
  }

  changeMainTitle(title) {
    this.setState({
      mainTitle: title
    });
  }

  changeContentMode(mode){
    this.setState({
      contentMode: mode
    });
  }

  setupMainToolbarActionButtons(isVisible){
    if (isVisible) {
      var actionButtons = document.querySelectorAll('.action-button');
      for (var i in actionButtons){
        if (!actionButtons[i].classList) continue;
        actionButtons[i].classList.add('visible');
        actionButtons[i].classList.remove('hidden');
      }
    } else {
      var actionButtons = document.querySelectorAll('.action-button');
      for (var i in actionButtons){
        if (!actionButtons[i].classList) continue;
        actionButtons[i].classList.add('hidden');
        actionButtons[i].classList.remove('visible');
      }
    }
  }

  render() {
    var _HomeServices = () => <HomeServices setupMainToolbarActionButtons={this.setupMainToolbarActionButtons} contentMode={this.state.contentMode} changeMainTitle={this.changeMainTitle.bind(this)} />,
        _Authenticate = () => <Authenticate setupMainToolbarActionButtons={this.setupMainToolbarActionButtons} contentMode={this.state.contentMode} changeMainTitle={this.changeMainTitle.bind(this)}/>,
        _Users = () => <Users setupMainToolbarActionButtons={this.setupMainToolbarActionButtons} contentMode={this.state.contentMode} changeMainTitle={this.changeMainTitle.bind(this)}/>,
        _Services = () => <Services setupMainToolbarActionButtons={this.setupMainToolbarActionButtons} contentMode={this.state.contentMode} changeMainTitle={this.changeMainTitle.bind(this)}/>,
        _ServiceDetail = ({ match }) => <ServiceDetail match={ match } changeMainTitle={this.changeMainTitle.bind(this)}/>,
        _ManageClients = () => <ManageClients setupMainToolbarActionButtons={this.setupMainToolbarActionButtons} contentMode={this.state.contentMode}/>;

    return (
      <HashRouter history={browserHistory}>
        <div className="main-body">
          <SideMenu isMenuOpen={ this.state.isMenuOpen }/>
          <div className="main-content">
            <Toolbar isActionButtonsVisible={ this.state.isActionButtonsVisible } mainTitle={this.state.mainTitle} changeContentMode={this.changeContentMode.bind(this)} onClickMenu={this.toggleSideMenu.bind(this)}/>
            <Route exact path="/" render={ _HomeServices }/>
            <Route path="/schedule" render={ () => <h3>Em Construção</h3> }/>
            <Route path="/login" render={ _Authenticate }/>
            <Route path="/users" render={ _Users }/>
            <Route exact path="/services" render={ _Services }/>
            <Route path="/services/:id"  render={ _ServiceDetail }/>
            <Route path="/clients" render={ _ManageClients }/>
          </div>
        </div>
      </HashRouter>
    )
  }
}

ReactDOM.render(
  <Provider store={store}>
    <Schedy isMenuOpen={true}/>
  </Provider>,
  document.getElementById('app')
);

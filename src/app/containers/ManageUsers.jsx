import React from 'react';
import { connect } from 'react-redux';
import { postUser, getUsers } from '../actions/users.js';
import ManageUsers from '../components/user/Manage.jsx';

const mapDispatchToProps = (dispatch) => {
  return {
    postUser: (token, username, email) => {
        dispatch(postUser(token, username, email));
    },
    getUsers: (token) => {
        dispatch(getUsers(token));
    }
  }
}

const mapStateToProps = (state) => ({
    'credential': state.credential,
    'users': state.users
});

const Users = connect(mapStateToProps, mapDispatchToProps)(ManageUsers)
export default Users



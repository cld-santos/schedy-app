import React from 'react';
import { connect } from 'react-redux';
import { putService, postService, getServices, getProfessionals } from '../actions/service.js';
import { getClients } from '../actions/clients.js';
import ManageServices from '../components/services/Manage.jsx';

const mapDispatchToProps = (dispatch) => {
  return {
    getServices: (token) => {
      dispatch(getServices(token));
    },
    putService: (token, service) => {
      dispatch(putService(token, service));
    },
    postService: (token, service) => {
      dispatch(postService(token, service));
    },
    getProfessionals: (token) => {
      dispatch(getProfessionals(token));
    },
    getClients: (token) => {
      dispatch(getClients(token));
    }
  }
}

const mapStateToProps = (state) => ({
  'credential': state.credential,
  'users': state.users,
  'service': state.service,
  'client': state.client,
  'servicesByProfessional': state.servicesByProfessional
});

const Services = connect(mapStateToProps, mapDispatchToProps)(ManageServices)
export default Services

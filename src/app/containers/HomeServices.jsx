import React from 'react';
import { connect } from 'react-redux';
import { getServicesByProfessional } from '../actions/service.js';
import Home from '../components/Home.jsx';

const mapDispatchToProps = (dispatch) => {
  return {
    getServicesByProfessional: (token, professionalId) => {
      dispatch(getServicesByProfessional(token, professionalId));
    }
  }
}
const mapStateToProps = (state) => ({
  'credential': state.credential,
  'servicesByProfessional': state.servicesByProfessional
});

const HomeServices = connect(mapStateToProps, mapDispatchToProps)(Home)

export default HomeServices;
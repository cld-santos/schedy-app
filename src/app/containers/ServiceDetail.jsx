import React from 'react';
import { connect } from 'react-redux';
import { putService, postService, getServices, getService, getProfessionals } from '../actions/service.js';
import { getClients } from '../actions/clients.js';
import ServiceDetail from '../components/services/Detail.jsx';

const mapDispatchToProps = (dispatch) => {
  return {
    putService: (token, service) => {
      dispatch(putService(token, service));
    },
    postService: (token, service) => {
      dispatch(postService(token, service));
    },
    getService: (id, token) => {
      dispatch(getService(id, token));
    },
    getServices: (token) => {
      dispatch(getServices(token));
    },
    getProfessionals: (token) => {
      dispatch(getProfessionals(token));
    },
    getClients: (token) => {
      dispatch(getClients(token));
    }
  }
}

const mapStateToProps = (state) => ({
  'credential': state.credential,
  'service': state.service
});

const Services = connect(mapStateToProps, mapDispatchToProps)(ServiceDetail)
export default Services

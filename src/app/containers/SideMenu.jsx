import React from 'react';
import { connect } from 'react-redux';
import { logout } from '../actions/auth.js';
import SideMenuComponent from '../components/SideMenu.jsx';

const mapDispatchToProps = (dispatch) => {
  return {
    onLogout: (username) => {
        dispatch(logout(username));
    }
  }
}
const mapStateToProps = (state) => ({
    'credential': state.credential
});

const SideMenu = connect(mapStateToProps, mapDispatchToProps)(SideMenuComponent)
export default SideMenu


import React from 'react';
import { connect } from 'react-redux';
import _toolbar from '../components/Toolbar.jsx';

const mapStateToProps = (state) => ({
  'schedy': state.schedy
});

const Toolbar = connect(mapStateToProps)(_toolbar)
export default Toolbar

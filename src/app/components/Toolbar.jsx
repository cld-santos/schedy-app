import React, { Component } from 'react';
import { Switch, Redirect, Link, BrowserRouter as Router, Route } from 'react-router-dom';
import { MDCToolbar, MDCToolbarFoundation } from '@material/toolbar';
import { MDCPersistentDrawer, MDCPersistentDrawerFoundation, util } from '@material/drawer';
import { MDCSimpleMenu, MDCSimpleMenuFoundation } from '@material/menu';

import BaseModal from './Modal.jsx';
import Authenticate from '../containers/Authenticate.jsx';
import Users from '../containers/ManageUsers.jsx';
import Services from '../containers/Services.jsx';
import ManageClients from '../containers/ManageClients.jsx';
import HomeServices from '../containers/HomeServices.jsx';

class MainMenu extends Component {

  constructor(props) {
    super(props);
    this.onClickMenu = props.onClickMenu;
    this.changeContentMode = props.changeContentMode;

    this.state = {
      mainTitle: props.mainTitle,
      isActionButtonsVisible: props.isActionButtonsVisible
    }
  }

  componentDidMount() {
    var self = this;
    MDCToolbar.attachTo(document.querySelector('.mdc-toolbar'));
    document.querySelector('#main-menu').addEventListener('click', function(event) {
      self.onClickMenu();
    });
    document.querySelector('.action-button.list').addEventListener('click', function(event) {
      self.changeContentMode('list');
    });
    document.querySelector('.action-button.add').addEventListener('click', function(event) {
      self.changeContentMode('add');
    });
  } 

  render() {
    var actionButtons = null;
    return (
      <header className="mdc-toolbar mdc-toolbar--fixed mdc-elevation--z4">
        <div className="mdc-toolbar__row">
          <section className="mdc-toolbar__section mdc-toolbar__section--align-start" role="toolbar">
            <a id="main-menu" className="material-icons">menu</a>
            <span className="mdc-toolbar__title">{this.props.schedy.hasOwnProperty('title') ? this.props.schedy.title : this.props.mainTitle }</span>
          </section>
          <section className="main-action-buttons mdc-toolbar__section mdc-toolbar__section--align-end"  role="toolbar">
            <a className="action-button list material-icons mdc-toolbar__icon" aria-label="Lista" alt="Lista">list</a>
            <a className="action-button add material-icons mdc-toolbar__icon" aria-label="Novo" alt="Novo">add</a>
          </section>
        </div>
      </header>    
    )
  }


}

export default MainMenu;

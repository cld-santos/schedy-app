import React, { Component } from 'react';
import {MDCTextfield, MDCTextfieldFoundation} from '@material/textfield';
import Modal from './Modal.jsx';

class ChangePassword extends Component {

  constructor(props) {
    super(props);
    
    this.changeUserPassword = props.changeUserPassword;    
    this.handleChange = this.handleChange.bind(this);    
    this.onOk = this.onOk.bind(this);    
  }

  show() {
    this.modal.show();
  }

  handleChange(event){
    const name = event.target.name;
    this.setState({ [name]: event.target.value });
  }

  onOk(){
    let credential = this.props.credential;
    this.changeUserPassword(
      credential.token,
      {
        id: credential.id,
        username: credential.username,
        password: this.state.pwda
      });
    this.closeModal();
  }

  render() {
    return (
      <div>
        <Modal onOk={() => {this.onOk()}} onCancel={function() {console.log('cancelou');}} isOpen={this.props.isOpen} title="Altere sua Senha">
          <form>
            <section className="mdc-card__supporting-text">
              <div className='mdc-textfield change_password'>
                <input id='change_password' type='password' className='mdc-textfield__input' name='pwda' onChange={this.handleChange}/>
                <label className='mdc-textfield__label' htmlFor='change_password'>Senha</label>
              </div>
              <div className='mdc-textfield confirmed_password'>
                <input id='confirmed_password' type='password' className='mdc-textfield__input' name='pwdb' onChange={this.handleChange}/>
                <label className='mdc-textfield__label' htmlFor='confirmed_password'>Confirmar Senha</label>
              </div>
            </section>
          </form>
        </Modal>
      </div>
    )
  }

  componentDidMount() {
    MDCTextfield.attachTo(document.querySelector('.change_password'));
    MDCTextfield.attachTo(document.querySelector('.confirmed_password'));
  } 

}

export default ChangePassword;





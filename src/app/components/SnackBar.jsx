import React, { Component } from 'react';
import {MDCSnackbar, MDCSnackbarFoundation} from '@material/snackbar';

class SnackBar extends Component {

  render() {
    if (this.snackbar && this.props.message && this.props.isSnackBarEnabled){
      var dataObj = {
        message: this.props.message,
        timeout: 2750
      };

      this.snackbar.show(dataObj);
    }
    return (
      <div className="mdc-snackbar mdc-snackbar--align-middle">
        <div className="mdc-snackbar__text"></div>
        <div className="mdc-snackbar__action-wrapper">
          <button type="button" className="mdc-button mdc-snackbar__action-button"></button>
        </div>
      </div>
    )
  }

  componentDidMount() {
    this.snackbar = MDCSnackbar.attachTo(document.querySelector('.mdc-snackbar'));
  } 

}

export default SnackBar;

import React from 'react';
import { Redirect } from 'react-router-dom'
import {MDCTextfield, MDCTextfieldFoundation} from '@material/textfield';

class Login extends React.Component {

  constructor(props){
    super(props);
    this.state = {
      'username': '',
      'password': ''
    };
    this.attemptToConnect = false;
    this.handleChange = this.handleChange.bind(this);
    this.handleLogin = this.handleLogin.bind(this);
    this.authenticateMe = props.authenticateMe;
    this.changeMainTitle = props.changeMainTitle;
    this.setupMainToolbarActionButtons = props.setupMainToolbarActionButtons;
  }

  handleLogin(event){
    event.preventDefault();
    this.attemptToConnect = true;
    this.authenticateMe(this.state.username, this.state.password);
  }

  handleChange(event){
    const name = event.target.name;
    this.setState({ [name]: event.target.value });
  }

  render(){
    var nonAuthenticatedAlert = null;
    var _credential = this.props.credential;

    if (_credential && _credential.authenticated){
      return <Redirect to={{pathname: '/'}}/>
    }

    if(_credential && this.attemptToConnect && !_credential.authenticated) {
      nonAuthenticatedAlert = <span className="mdc-typography--subheading1 error">oops, não te identificamos, tente novamente.</span>
    }


    return (
      <div className="content">
        <div className="users-grid mdc-layout-grid">
          <div className="mdc-layout-grid__inner">
          
            <div className="mdc-card login-card">
              <form onSubmit={this.handleLogin}>
                  <section className="mdc-card__primary login-card">
                    <h1 className="mdc-card__title mdc-card__title--large">Informe suas Credenciais</h1>
                    
                  </section>
                  <section className="mdc-card__supporting-text login-card">
                    <div className='mdc-textfield login'>
                      <input id='login' type='text' className='mdc-textfield__input' name='username' value={this.state.username} onChange={this.handleChange}/>
                      <label className='mdc-textfield__label' htmlFor='login'>Nome Usuário</label>
                    </div>
                    <div className='mdc-textfield password'>
                      <input id='password' className='mdc-textfield__input' name='password' value={this.state.password} onChange={this.handleChange} type='password'/>
                      <label className='mdc-textfield__label' htmlFor='password'>Senha</label>
                    </div>
                  </section>
                  <section className="mdc-card__actions login-card">
                    <button className='mdc-button' type='submit'>Autenticar</button>
                    {nonAuthenticatedAlert}
                  </section>
              </form>
            </div>
          </div>
        </div>
      </div>
    )
  }

  componentDidMount() {
    MDCTextfield.attachTo(document.querySelector('.login'));
    MDCTextfield.attachTo(document.querySelector('.password'));
    this.changeMainTitle('Schedy');
    this.setupMainToolbarActionButtons(false);
  }
}

export default Login;


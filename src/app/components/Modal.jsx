import React, { Component } from 'react'
import {MDCDialog, MDCDialogFoundation, util} from '@material/dialog';

class Modal extends Component {

  constructor(props){
    super(props);

    this.title = props.title;
    this.modal = null;
    this.onOk = props.onOk;
    this.onCancel = props.onCancel;
    this.ind = 0;
  }

  componentDidMount() {
    var self = this;
    this.modal = MDCDialog.attachTo(document.querySelector('#my-mdc-dialog'));
    this.modal.listen('MDCDialog:accept', function() {
      self.onOk();
    })

    this.modal.listen('MDCDialog:cancel', function() {
      self.onCancel();
    })    
  }

  render() {
    if (this.ind !== this.props.isOpen) {
      this.modal.show();
      this.ind += 1;
    }

    return (
      <aside id="my-mdc-dialog" className="mdc-dialog" role="alertdialog">
        <div className="mdc-dialog__surface">
          <header className="mdc-dialog__header">
            <h2 id="my-mdc-dialog-label" className="mdc-dialog__header__title">
              {this.title}
            </h2>
          </header>
          <section id="my-mdc-dialog-description" className="mdc-dialog__body">
            {this.props.children}
          </section>
          <footer className="mdc-dialog__footer">
            <button type="button" className="mdc-button mdc-dialog__footer__button mdc-dialog__footer__button--cancel">Cancelar</button>
            <button type="button" className="mdc-button mdc-dialog__footer__button mdc-dialog__footer__button--accept">Ok!</button>
          </footer>
        </div>
        <div className="mdc-dialog__backdrop"></div>
      </aside>
    )
  }
}

export default Modal

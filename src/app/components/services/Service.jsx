import React from 'react'
import { Link } from 'react-router-dom';


class Service extends React.Component {

  constructor(props) {
    super(props);
  }

  render() {
    var service = this.props.service;

    return (
      <div className="mdc-layout-grid__cell mdc-layout-grid__cell--span-6">
        <div className="mdc-card services">
          <section className="mdc-card__primary services"> 
          </section>
          <section className="mdc-card__primary">
            <h1 className="mdc-card__title">Eng.: { service.professional.username }</h1>
            <h2 className="mdc-card__title--large">Título: { service.title }</h2>
            Criado por: {service.user_created.username}, em { service.date_created }              
          </section>
          <section className="mdc-card__actions ">
            <Link className="mdc-button mdc-button--compact mdc-card__action" to={"/services/" + service.id} >
              detalhes
            </Link>
          </section>
        </div>
      </div>)
  }
}

export default Service

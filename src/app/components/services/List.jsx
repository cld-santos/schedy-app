import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import Service from './Service.jsx';

class ServiceList extends React.Component {

  constructor(props){
    super(props);
    this.putService = props.putService;
  }

  render(){

    let services = this.props.service && this.props.service.items ? this.props.service.items : [];

    return (
      <div className="services-grid mdc-layout-grid">
        <br/>
        <div className="mdc-layout-grid__inner">
          {services.map(
            service => (
              <Service 
                service={service}
                putService={this.putService}
                key={service.id}/>
            )
          )}
        </div>
      </div>
    )
  }
}

export default ServiceList;
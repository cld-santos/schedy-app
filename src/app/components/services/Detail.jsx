import React from 'react'
import { Link } from 'react-router-dom';
import ManageNotes from '../../containers/ManageNotes.jsx';


class Service extends React.Component {

  constructor(props) {
    super(props);
    this.serviceId = parseInt(props.match.params.id);

    let credential = this.props.credential;
    if (credential && credential.authenticated) {
      this.token = credential.token;
      props.getService(this.serviceId, this.token);
    }
    this.state = {
      'textOverflow': 'text-overflow'
    }
  }

  render() {
    var serviceComponent = null,
      serviceSummary = null,
      service = this.props.service.item;

    if (service && this.serviceId === service.id) {
      serviceComponent = <div className={'service-detail ' + this.state.textOverflow }> { service.description } Criado por: { service.user_created.username }, em { service.date_created } </div>

      serviceSummary = (
        <div className='content-body'>
          <div className='mdc-layout-grid mdc-elevation--z1 service-description'>
            { serviceComponent }
          </div>
          <ManageNotes serviceId={this.serviceId}></ManageNotes>
        </div>
      );
    }
    return serviceSummary;
  }

  componentDidMount() {
    var toogleDescription = function(event) {
      this.setState({'textOverflow': this.state.textOverflow ? null : 'text-overflow' });
    }
    var _description = document.querySelector('.service-description');
    if (_description)
      _description.addEventListener('click', toogleDescription.bind(this));
  }
}

export default Service

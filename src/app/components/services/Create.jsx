import React from 'react';
import { Redirect } from 'react-router-dom'
import {MDCTextfield, MDCTextfieldFoundation} from '@material/textfield';
import {MDCSelect, MDCSelectFoundation} from '@material/select';
import SnackBar from '../SnackBar.jsx';

class CreateService extends React.Component {

  constructor(props){
    super(props);

    this.state = {
      'userCreatedId': props.credential.id,
      'professionalId': '',
      'clientId': '',
      'title': '',
      'description': '',
      'status': 'CREATED',
      'options': undefined
    }

    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.postService = props.postService;
    this.isSnackBarEnabled = false;
  }

  onSubmit(event){
    this.isSnackBarEnabled = true;
    this.postService(
      this.props.credential.token, {
        'userCreatedId': this.state.userCreatedId,
        'professionalId': this.state.professionalId,
        'clientId': this.state.clientId,
        'title': this.state.title,
        'description': this.state.description,
        'status': this.state.status,
      }
    );
    event.preventDefault();
  }

  onChange(event){
    this.setState({ 
      [event.target.name]: event.target.value
    });
    event.preventDefault();
  }

  render() {
    if (this.props.professionals) {
      this.professionals = this.props.professionals.map((p) => 
        <li className="mdc-list-item" role="option" id={p.id}>
          {p.username}
        </li>
      )
    }

    if (this.props.clients) {
      this.clients = this.props.clients.map((c) =>
        <li className="mdc-list-item" role="option" id={c.id}>
          {c.name}
        </li>
      )
    }

    var component = (
      <div className="content-body">
        <div className="clients-grid mdc-layout-grid mdc-elevation--z4">
          <form onSubmit={this.onSubmit}>
            <div className="mdc-layout-grid__inner">
              <div className="mdc-layout-grid__cell mdc-layout-grid__cell--span-8">
                <div className='mdc-textfield form-field title'>
                  <input id='id_title' type='text' className='mdc-textfield__input' name='title' onChange={this.onChange} value={this.state.title}/>
                  <label className='mdc-textfield__label' htmlFor='title'>Título</label>
                </div>
              </div>
            </div>
            <div className="mdc-layout-grid__inner">
              <div className="mdc-layout-grid__cell mdc-layout-grid__cell--span-4">
                <div className="mdc-select professionals" role="listbox" tabIndex="0">
                  <span className="mdc-select__selected-text">Profissionais</span>
                  <div className="mdc-simple-menu mdc-select__menu">
                    <ul className="mdc-list mdc-simple-menu__items">
                      {this.professionals}
                    </ul>
                  </div>
                </div>
              </div>
              <div className="mdc-layout-grid__cell mdc-layout-grid__cell--span-4">
                <div className="mdc-select clients" role="listbox" tabIndex="0">
                  <span className="mdc-select__selected-text">Clientes</span>
                  <div className="mdc-simple-menu mdc-select__menu">
                    <ul className="mdc-list mdc-simple-menu__items">
                      {this.clients}
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            <div className="mdc-layout-grid__inner">
              <div className="mdc-layout-grid__cell mdc-layout-grid__cell--span-8">
                <div className="mdc-textfield mdc-textfield--multiline form-field description">
                  <textarea id='id_description' type='text' className='mdc-textfield__input' name='description' onChange={this.onChange} rows='8'/>
                  <label htmlFor="id_description" className="mdc-textfield__label">Descrição</label>
                </div>            
              </div>
            </div>
            <button className='mdc-button mdc-button--raised mdc-button--dense' type='submit'>Cadastrar</button>
            <SnackBar isSnackBarEnabled={this.isSnackBarEnabled} message={this.props.errorMessage}/>
          </form>
        </div>
      </div>
    );
    this.isSnackBarEnabled = false;
    return component;
  }

  componentDidMount() {
    var selectProfessionals = new MDCSelect(document.querySelector('.professionals'));
    selectProfessionals.listen('MDCSelect:change', (value) => {
      this.setState({'professionalId': parseInt(selectProfessionals.value)})
    }); 

    var selectClients = new MDCSelect(document.querySelector('.clients'));
    selectClients.listen('MDCSelect:change', (value) => {
      this.setState({'clientId': parseInt(selectClients.value)})
    }); 

    MDCTextfield.attachTo(document.querySelector('.title'));
    MDCTextfield.attachTo(document.querySelector('.description'));
  }

}

export default CreateService;

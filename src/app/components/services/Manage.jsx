import React from 'react';
import { Redirect } from 'react-router-dom';
import CreateService from './Create.jsx';
import ServiceList from './List.jsx';

class ManageServices extends React.Component {

  constructor(props){
    super(props);
    this.postService = props.postService;
    this.putService = props.putService;

    let credential = this.props.credential;
    if (credential && credential.authenticated){
      this.token = credential.token;
      this.state = {
        'credential': credential,
        'listMode': true
      }

      this.props.getServices(this.token);
      this.props.getProfessionals(this.token);
      this.props.getClients(this.token);

    }

    this.changeMainTitle = props.changeMainTitle;
    this.setupMainToolbarActionButtons = props.setupMainToolbarActionButtons;
  }

  render() {
    if (!this.props.credential.authenticated){
      return <Redirect to={{pathname: '/login'}}/>
    }

    this.service = this.props.service.items ? this.props.service : this.service;
    this.professionals = this.props.service.professionals ? this.props.service.professionals : this.professionals;
    this.clients = this.props.client.items ? this.props.client.items : this.clients;
    
    var errorMessage = '';
    if (this.props.service && this.props.service.hasOwnProperty('created') &&
        !this.props.service.created){
      errorMessage = "Não foi possível criar o serviço.";
    }

    if (this.props.service && this.props.service.hasOwnProperty('created') &&
        this.props.service.created){
      errorMessage ="Serviço criado com sucesso.";
    }

    var onlyIfCombosAreLoaded = this.professionals && this.clients && this.service;
    if (onlyIfCombosAreLoaded) {
      var serviceBody = null;
      switch (this.props.contentMode){
        case 'add':
          serviceBody = (<CreateService
            credential={this.state.credential}
            professionals={this.professionals}
            clients={this.clients}
            postService={this.postService}
            errorMessage={errorMessage}/>);
          break;
        case 'list':
          serviceBody = (
            <div className="content-body">
              <ServiceList 
                putService={this.putService}
                service={this.service}
                contentMode={this.props.contentMode}/>
            </div>);
          break;
      }

      return serviceBody;

    } else {
      return (
        <div className="content-body">
          <h1>Carregando..</h1>
        </div>
      )
    }
  }

  componentDidMount() {
    this.changeMainTitle('Serviços');
    this.setupMainToolbarActionButtons(true);
  }

}

export default ManageServices;

import React from 'react';
import { Redirect } from 'react-router-dom'
import { MDCTextfield, MDCTextfieldFoundation } from '@material/textfield';
import { MDCSelect, MDCSelectFoundation } from '@material/select';
import SnackBar from '../SnackBar.jsx';
import Note from './Note.jsx';


class CreateNote extends React.Component {

  constructor(props){
    super(props);

    this.state = {
      'description': ''
      // 'files': '',
      // 'pictures': '',
    }
    let credential = this.props.credential;
    if (credential && credential.authenticated) {
      this.token = credential.token;
      props.getNotesByService(this.props.service.item, this.token);
    }
    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.postNote = props.postNote;
    this.isSnackBarEnabled = false;
  }

  onSubmit(event){
    this.isSnackBarEnabled = true;
    this.postNote(
      this.props.credential.token, {
        'user_created': this.props.credential.id,
        'service': this.props.service.hasOwnProperty('item') ? this.props.service.item.id : 0,
        'description': this.state.description
        // 'files': this.state.files,
        // 'pictures': this.state.pictures,
      }
    );
    event.preventDefault();
  }

  onChange(event){
    this.setState({ 
      [event.target.name]: event.target.value
    });
    event.preventDefault();
  }

  render() {
    var message = null;
    if (this.props.note && !this.props.note.created)
      message = 'Não foi possível criar a nota.';

    if (this.props.note && this.props.note.created)
      message = 'Nota criada com sucesso!';

    var _notes = <div></div>;

    if (this.props.note.items) {
      _notes = this.props.note.items
        .map(
          note => (
              <Note note={ note } key={note.id}/>
          )
        )
    }
    var component = (
      <div>
        <div className='mdc-layout-grid'>
          <form onSubmit={this.onSubmit}>
            <div className='mdc-card'>
              <section className='mdc-card__supporting-text note-card'>
                <div className="mdc-textfield mdc-textfield--multiline note">
                  <textarea id='description' type='text' className='mdc-textfield__input' name='description' onChange={this.onChange} rows='8' cols='100'/>
                  <label htmlFor="description" className="mdc-textfield__label">o que há de novo?</label>
                </div>            
              </section>
              <section className='mdc-card__actions note-card'>
                <section className='mdc-toolbar__section mdc-toolbar__section--align-start' role='toolbar'>
                  <a className='action-button inverse list material-icons mdc-toolbar__icon' aria-label='Lista' alt='Adicionar fotos'>image</a>
                  <a className='action-button inverse add material-icons mdc-toolbar__icon' aria-label='Novo' alt='Adicionar Documentos'>attach_file</a>
                </section>
                <section className='main-action-buttons mdc-toolbar__section mdc-toolbar__section--align-end' role='toolbar'>
                  <button className='mdc-button mdc-button--compact mdc-card__action' type='submit'>Create</button>
                </section>
              </section>
            </div>
          </form>
          <SnackBar isSnackBarEnabled={ this.isSnackBarEnabled } message={ message }/>
        </div>
        <div className="mdc-layout-grid">
          <div className="mdc-layout-grid__inner">
            {_notes}
          </div>
        </div>
      </div>
    );
    this.isSnackBarEnabled = false;
    return component;
  }

  componentDidMount() {
    MDCTextfield.attachTo(document.querySelector('.note'));
  }

}

export default CreateNote;

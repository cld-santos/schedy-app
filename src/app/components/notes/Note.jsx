import React from 'react';
import { Redirect } from 'react-router-dom'
var Markdown = require('react-markdown');

class Note extends React.Component {

  constructor(props){
    super(props);
    this.note = props.note;
  }

  render() {
    var component = (
      <div className="mdc-layout-grid__cell mdc-layout-grid__cell--span-12">
        <div className='mdc-card main-note'>
          <section className='mdc-card__supporting-text note-card'>
            <Markdown source={this.note.description} />
          </section>
          <section className='mdc-card__actions note-card'>
            <section className='mdc-toolbar__section mdc-toolbar__section--align-start' role='toolbar'>
            </section>
            <section className='main-action-buttons mdc-toolbar__section mdc-toolbar__section--align-end' role='toolbar'>
              <span className="mdc-typography--body1 mdc-typography--adjust-margin">
                {this.note.user_created.username}, em { this.note.date_created }
              </span>
            </section>
          </section>
        </div>
      </div>
    );
    return component;
  }

}

export default Note;

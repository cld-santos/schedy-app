import React, { Component } from 'react';
import { Redirect, Link } from 'react-router-dom';
import { MDCToolbar, MDCToolbarFoundation } from '@material/toolbar';
import { MDCPersistentDrawer, MDCPersistentDrawerFoundation, util } from '@material/drawer';
import { MDCSimpleMenu, MDCSimpleMenuFoundation } from '@material/menu';
import { MDCTextfield, MDCTextfieldFoundation } from '@material/textfield';

import ChangePwd from '../containers/ChangePassword.jsx';

class SideMenuComponent extends Component {

  constructor(props) {
    super(props);
    this.state = {
      isChangePasswordOpen: 0,
      isMenuOpen: props.isMenuOpen
    }

    this.logout = props.onLogout;
    this.mainMenu = null;
  }

  render() {
    if (this.mainMenu && this.props.credential && this.props.credential.authenticated){
      this.mainMenu.open = this.props.isMenuOpen;
    }

    return (
      <aside className="mdc-persistent-drawer">
        <nav className="mdc-persistent-drawer__drawer">
          <div className="mdc-card-background">
            <div className="mdc-card">
              <section className="mdc-card__primary">
                <h1 className="mdc-card__title mdc-card__title--large"></h1>
                <h2 className="mdc-card__subtitle"></h2>
              </section>
              <section className="mdc-card__actions">
                <button id='menu' className="mdc-fab mdc-fab--mini material-icons" aria-label="person">
                  <span className="mdc-fab__icon">
                    person
                  </span>
                </button>
                <div className="mdc-simple-menu mdc-simple-menu--open-from-top-right" tabIndex="-1">
                  <ul className="mdc-simple-menu__items mdc-list" role="menu" aria-hidden="true">
                    <li className="mdc-list-item change-password" role="menuitem" tabIndex="0">
                      Alterar Senha
                    </li>
                    <li className="mdc-list-item logout" role="menuitem" tabIndex="0">
                      Sair
                    </li>
                  </ul>
                </div>              
              </section>
            </div>
          </div>
          <nav className="mdc-persistent-drawer__content mdc-list">
            <Link className="mdc-list-item" to="/">
              <i className="material-icons mdc-list-item__start-detail" aria-hidden="true">home</i>Meus Serviços
            </Link>
            <Link className="mdc-list-item" to="/schedule">
              <i className="material-icons mdc-list-item__start-detail" aria-hidden="true">schedule</i>Agenda
            </Link>
            <Link className="mdc-list-item" to="/services">
              <i className="material-icons mdc-list-item__start-detail" aria-hidden="true">receipt</i>Serviços
            </Link>
            <Link className="mdc-list-item" to="/clients"> 
              <i className="material-icons mdc-list-item__start-detail" aria-hidden="true">people</i>Clientes
            </Link>
          </nav>
          <div className="mdc-temporary-drawer__toolbar-spacer"></div>
          <nav className="mdc-persistent-drawer__content mdc-list">
            <Link className="mdc-list-item" to="/users">
              <i className="material-icons mdc-list-item__start-detail" aria-hidden="true">settings</i>Usuários
            </Link>
          </nav>
        </nav>
        <ChangePwd isOpen={this.state.isChangePasswordOpen}/>
      </aside>
    )
  }

  componentDidMount() {
    var self = this;
    this.mainMenu = MDCPersistentDrawer.attachTo(document.querySelector('.mdc-persistent-drawer'));    
    this.mainMenu.open = this.props.isMenuOpen;
    let menu = MDCSimpleMenu.attachTo(document.querySelector('.mdc-simple-menu'));

    document.querySelector('#menu').addEventListener('click', function(event) {
      menu.open = !menu.open;
    });
    document.querySelector('.change-password').addEventListener('click', function(event) {
      var newIndice = self.state.isChangePasswordOpen + 1;
      self.setState({ isChangePasswordOpen:  newIndice });
    });
    document.querySelector('.logout').addEventListener('click', function(event) {
      self.mainMenu.open = false;
      self.logout();
    });

  } 

}

export default SideMenuComponent;




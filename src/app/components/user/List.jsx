import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import User from './User.jsx';

class UserList extends React.Component {

  constructor(props){
    super(props);

    this.onRemoveUser = function(user) {
      console.log('removeu o ' + user);
    }
  }

  onRemove(username){
    alert('Removendo o usuário ' + username);
  }

  render(){
    let users = this.props.users || [];
    return (
      <div>
        <div className="users-grid mdc-layout-grid">
          <div className="mdc-layout-grid__inner">
            { 
              users.map(
                user => (
                  <User key={user.id}
                        username={user.username} 
                        email={user.email} 
                        tipo={user.tipo} 
                        onRemoveUser={() => (this.onRemove(user.username))}/>
                )
              )
            }
          </div>
        </div>
      </div>
    )
  }
}

export default UserList;
import React from 'react'


class User extends React.Component {

  constructor(props) {
    super(props);
    this.onRemoveUser = props.onRemoveUser;
  }

  render() {
    return (
      <div className="mdc-layout-grid__cell mdc-layout-grid__cell--span-4">
        <div className="mdc-card users">
          <section className="mdc-card__primary users"> 
          </section>
          <section className="mdc-card__primary">
            <h2 className="mdc-card__title--large">{this.props.username}</h2>
            <h1 className="mdc-card__title">{this.props.email}</h1>
            {this.props.tipo}
          </section>
          <section className="mdc-card__actions ">
            <button className="mdc-button mdc-button--compact mdc-card__action" onClick={() => this.onRemoveUser(this.props.username)}>remove</button>
          </section>
        </div>
      </div>      
    )
  }
}

export default User

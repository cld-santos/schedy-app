import React from 'react';
import { Redirect } from 'react-router-dom'
import {MDCTextfield, MDCTextfieldFoundation} from '@material/textfield';
import {MDCSelect, MDCSelectFoundation} from '@material/select';
import SnackBar from '../SnackBar.jsx';

class CreateUser extends React.Component {

  constructor(props){
    super(props);

    this.token = props.token;
    this.postUser = props.postUser;

    this.state = {
      'username': '',
      'email': '',
      'tipo':''
    };
    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.isSnackBarEnabled = false;
  }

  onSubmit(event){
    this.postUser(this.token, { 
      username: this.state.username,
      email:this.state.email,
      tipo:this.tipo
    });
    this.isSnackBarEnabled = true;
    event.preventDefault();
  }

  onChange(event){
    this.setState({ [event.target.name]: event.target.value });
  }

  render(){
    var component = (
      <div className="content-body">
        <div className="clients-grid mdc-layout-grid mdc-elevation--z4">
          <form onSubmit={this.onSubmit}>
            <div className="mdc-layout-grid__inner">
              <div className="mdc-layout-grid__cell mdc-layout-grid__cell--span-12">
                <div className='mdc-textfield username'>
                  <input id='id_username' type='text' className='mdc-textfield__input' name='username' onChange={this.onChange} value={this.state.username}/>
                  <label className='mdc-textfield__label' htmlFor='username'>Nome</label>
                </div>
                <div className='mdc-textfield email'>
                  <input id='id_email' type='text' className='mdc-textfield__input' name='email' onChange={this.onChange} value={this.state.email}/>
                  <label className='mdc-textfield__label' htmlFor='email'>e-mail</label>
                </div>
                <div className="mdc-select tipo" role="listbox" tabIndex="0">
                  <span className="mdc-select__selected-text">Tipo</span>
                  <div className="mdc-simple-menu mdc-select__menu">
                    <ul className="mdc-list mdc-simple-menu__items">
                      <li className="mdc-list-item" role="option" id="eng">
                        Engenheiro
                      </li>
                      <li className="mdc-list-item" role="option" id="ges">
                        Gestão
                      </li>
                    </ul>
                  </div>
                </div>
                <button className='mdc-button mdc-button--accent' type='submit'>Cadastrar</button>
                <SnackBar isSnackBarEnabled={this.isSnackBarEnabled} message={this.props.errorMessage}/>
              </div>
            </div>
          </form>
        </div>
      </div>
    );
    this.isSnackBarEnabled = false;
    return component;
  }

  componentDidMount() {
    var selectTipo = new MDCSelect(document.querySelector('.tipo'));
    selectTipo.listen('MDCSelect:change', (value) => {
      this.tipo = selectTipo.value;
    });

    MDCTextfield.attachTo(document.querySelector('.username'));
    MDCTextfield.attachTo(document.querySelector('.email'));
  }

}

export default CreateUser;
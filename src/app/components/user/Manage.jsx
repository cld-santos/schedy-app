import React from 'react'
import { Redirect } from 'react-router-dom'
import CreateUser from './Create.jsx'
import UserList from './List.jsx'

class ManageUsers extends React.Component {

  constructor(props){
    super(props);
    let credential = props.credential;

    if (credential && credential.authenticated){
      this.token = credential.token;
      props.getUsers(credential.token);
    }
    this.postUser = props.postUser;
    this.state = {
      'listMode': true
    }
    this.changeMainTitle = props.changeMainTitle;
    this.setupMainToolbarActionButtons = props.setupMainToolbarActionButtons;
  }

  render() {
    if (!this.props.credential.authenticated){
      return <Redirect to={{pathname: '/login'}}/>
    }

    var errorMessage = null;
    if (this.props.users && this.props.users.hasOwnProperty('created') && 
        !this.props.users.created) {
      errorMessage = "Não foi possível criar o usuário.";
    }

    if (this.props.users && this.props.users.hasOwnProperty('created') && 
        this.props.users.created) {
      errorMessage = "Usuário criado com sucesso.";
    }

    var userBody = null;
    switch (this.props.contentMode){
      case 'add':
        userBody = <CreateUser 
          postUser={this.postUser}
          token={this.token}
          errorMessage={errorMessage}/>;
        break;
      case 'list':
        userBody = <UserList users={this.props.users.items}/>;
        break;
    }
    
    return (
      <div className="content-body">
        {userBody}
      </div>
    )
  }

  componentDidMount() {
    this.changeMainTitle('Usuários');
    this.setupMainToolbarActionButtons(true);

  }

}

export default ManageUsers;

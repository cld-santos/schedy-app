import fetch from 'isomorphic-fetch';

export const createNote = (note) => ({
    type: 'CREATE_NOTE',
    item: note,
    created: true
});

export const failCreateNote = (note) => ({
    type: 'FAIL_CREATE_NOTE',
    title: note.title,
    created: false
});

export const listNotes = (notes) => ({
    type: 'LIST_NOTE',
    items: notes
});

export function postNote(token, note) {

  var formData = new FormData();
  formData.append('user_created', note.user_created);
  formData.append('service', note.service);
  formData.append('description', note.description);

  return dispatch => {
    return fetch('/api/notes/', {
      method: 'POST',
      body: formData,
      headers: {
        'Authorization': 'Token ' + token
      }
    }).then(function(response) {
      if (response.status == 201) {
        return response.json();
      } else {
        dispatch(failCreateNote(note));
      }
    }).then(function(json) {
      if (!json) return;
      dispatch(createNote(json));
    })
  };
}

export function getNotesByService(service, token) {
  return dispatch => {
    return fetch('/api/notes/?ordering=-id,service_id=' + service.id, {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Token ' + token
      }
    }).then(function(response) {
      if (response.status == 200) {
        return response.json();
      }
    }).then(function(notes) {
      if (!notes) return;
      dispatch(listNotes(notes.results));
    })
  };
}

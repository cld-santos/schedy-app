import { changeMainTitle } from './schedy.js';
import fetch from 'isomorphic-fetch';

export const listServicesByProfessional = (services) => ({
  'type': 'LIST_SERVICES_BY_PROFESSIONAL',
  'servicesByProfessional': services
});

export const listProfessionals = (professionals) => ({
  'type': 'LIST_PROFESSIONALS',
  'professionals': professionals
});

export const listServices = (service) => ({
  type: 'LIST_SERVICES',
  items: service
});
export const updateService = (service) => ({
  type: 'UPDATE_SERVICE',
  items: service,
});
export const createService = (service) => ({
  type: 'CREATE_SERVICE',
  items: service,
  created: true
});

export const _getService = (service) => ({
  type: 'GET_SERVICE',
  item: service
});

export const failCreateService = (service) => ({
  type: 'FAIL_CREATE_SERVICE',
  items: service,
  created: false
});
export const failUpdateService = (service) => ({
  type: 'FAIL_UPDATE_SERVICE',
  items: service,
  created: false
});

export function putService(token, service) {

  var formData = new FormData();
  formData.append('id', service.id);
  formData.append('userCreatedId', service.userCreatedId);
  formData.append('professionalId', service.professionalId);
  formData.append('clientId', service.clientId);
  formData.append('title', service.title);
  formData.append('description', service.description);
  formData.append('status', service.status);

  return dispatch => {
    return fetch('/api/services/'+ service.id, {
      method: 'PUT',
      body: formData,
      headers: {
        'Authorization': 'Token ' + token
      }
    }).then(function(response) {
      if (response.status == 201) {
        return response.json();
      } else {
        dispatch(failUpdateService(service));
      }
    }).then(function(json) {
      if (!json) return;
      dispatch(updateService(json));
    })
  };
}

export function postService(token, service) {

  var formData = new FormData();
  formData.append('userCreatedId', service.userCreatedId);
  formData.append('professionalId', service.professionalId);
  formData.append('clientId', service.clientId);
  formData.append('title', service.title);
  formData.append('description', service.description);
  formData.append('status', service.status);

  return dispatch => {
    return fetch('/api/services/', {
      method: 'POST',
      body: formData,
      headers: {
        'Authorization': 'Token ' + token
      }
    }).then(function(response) {
      if (response.status == 201) {
        return response.json();
      } else {
        dispatch(failCreateService(service));
      }
    }).then(function(json) {
      if (!json) return;
      dispatch(createService(json));
    })
  };
}

export function getService(id, token) {
  return dispatch => {
    return fetch('/api/services/' + id + '/', {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Token ' + token
      }
    }).then(function(response) {
      if (response.status == 200) {
        return response.json();
      }
    }).then(function(service) {
      if (!service) return;
      dispatch(changeMainTitle('Serviço: ' + service.title));
      dispatch(_getService(service));
    })
  };
}

export function getServices(token) {
  return dispatch => {
    dispatch(changeMainTitle('Lista de Serviços'));
    return fetch('/api/services/', {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Token ' + token
      }
    }).then(function(response) {
      if (response.status == 200) {
        return response.json();
      }
    }).then(function(json) {
      if (!json) return;
      dispatch(listServices(json.results));
    })
  };
}

export function getServicesByProfessional(token, professionalId) {
  let url = '/api/services/';
  if (professionalId) {
    url += '?professional_id=' + professionalId;
  }

  return dispatch => {
    dispatch(changeMainTitle('Meus Serviços'));
    return fetch(url, {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Token ' + token
      }
    }).then(function(response) {
      if (response.status == 200) {
        return response.json();
      }
    }).then(function(myServices) {
      if (!myServices) return;
      dispatch(listServicesByProfessional(myServices.results));
    })
  };
}

export function getProfessionals(token) {
  return dispatch => {
    return fetch('/api/users/', {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Token ' + token
      }
    }).then(function(response) {
      if (response.status == 200) {
        return response.json();
      }
    }).then(function(json) {
      if (!json) return;
      dispatch(listProfessionals(json.results));
    })
  };
}


export const changeMainTitle = (title) => ({
  type: 'TITLE_CHANGED',
  'title': title
});

export const changeViewMode = (mode) => ({
  type: 'VIEW_CHANGED',
  'mode': mode
});

import { changeMainTitle } from './schedy.js';
import fetch from 'isomorphic-fetch';

export const createUser = (user) => ({
    type: 'CREATE_USER',
    id: user.id,
    username: user.username,
    email: user.email,
    created: true
});
export const failCreateUser = (user) => ({
    type: 'FAIL_CREATE_USER',
    username: user.username,
    created: false
});

export const updateUser = (user) => ({
    type: 'UPDATE_USER',
    id: user.id,
    username: user.username,
    email: user.email,
    updated: true
});

export const listUsers = (users) => ({
    type: 'LIST_USER',
    items: users
});

export function putUser(token, user) {

  var formData = new FormData();

  formData.append('password', user.password);

  return dispatch => {
    return fetch('/api/users/' + user.id + '/change_password/', {
      method: 'POST',
      body: formData,
      headers: {
        'Authorization': 'Token ' + token
      }
    }).then(function(response) {
      if (response.status == 201) {
        return response.json();
      }
    }).then(function(json) {
      if (!json) return;
      dispatch(updateUser(json));
    })
  };
}


export function postUser(token, user) {

  var formData = new FormData();

  formData.append('username', user.username);
  formData.append('email', user.email);
  formData.append('password', 'none');

  return dispatch => {
    return fetch('/api/users/', {
      method: 'POST',
      body: formData,
      headers: {
        'Authorization': 'Token ' + token
      }
    }).then(function(response) {
      if (response.status == 201) {
        return response.json();
      } else {
        dispatch(failCreateUser(user));
      }
    }).then(function(json) {
      if (!json) return;
      dispatch(createUser(json));
    })
  };
}

export function getUsers(token) {
  return dispatch => {
    dispatch(changeMainTitle('Lista de Usuários'));
    return fetch('/api/users/', {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Token ' + token
      }
    }).then(function(response) {
      if (response.status == 200) {
        return response.json();
      }
    }).then(function(users) {
      if (!users) return;
      dispatch(listUsers(users.results));
    })
  };
}
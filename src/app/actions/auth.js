import fetch from 'isomorphic-fetch';

export const authenticated = (user) => ({
  type: 'AUTHENTICATED',
  id: user.id,
  username: user.username,
  token: user.token,
  authenticated: true
});

export const nonAuthenticated = (user) => ({
  type: 'NON_AUTHENTICATED',
  authenticated: false
});

export const logout = (username) => ({
  type: 'LOGOUT',
  authenticated: false
});

export function authenticateMe(username, password) {
  var formData = new FormData();
  formData.append('username', username);
  formData.append('password', password);
  return function(disparador) {
    return fetch('/api/token/', {
      method: 'POST',
      body: formData
    }).then(function(response) {
      if (response.status == 200) {
        return response.json();
      } else {
        var error = new Error(response.statusText);
        error.response = response;
        throw error;
      }
    }).then(function(json) {
      if (!json) return;
      disparador(authenticated(json));
    }).catch(function(error) {
      disparador(nonAuthenticated(error.response.json()));
    })
  };
}
